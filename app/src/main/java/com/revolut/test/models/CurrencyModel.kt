package com.revolut.test.models

import java.math.BigDecimal

/**
 * Author: Vincenzo Guarino
 * Model currency.
 */

data class CurrencyModel(

    val name: Int = 0,
    val symbol: String = "EUR",
    var rate: BigDecimal? = BigDecimal.ONE,
    val flag: Int = 0

)
fun CurrencyModel.calculateRate(baseRateUnit:BigDecimal): BigDecimal? {
    return rate?.times(baseRateUnit)
}

