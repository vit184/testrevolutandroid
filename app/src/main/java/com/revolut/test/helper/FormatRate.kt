package com.revolut.test.helper

import android.text.Editable
import android.widget.EditText
import org.json.JSONException
import org.json.JSONObject
import java.math.BigDecimal
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.ParseException
import java.util.*

/**
 * Author: Vincenzo Guarino
 * This class contains functions that are used to format the rate.
 */

class FormatRate {

    fun formatRate(baseRateUnit: BigDecimal):String{
        // the format depends from the current Locale as Revolut App does
        return  String.format(Locale.getDefault(),"%,.2f", baseRateUnit.setScale(2, RoundingMode.HALF_DOWN));
    }

    @Throws(JSONException::class)
    fun jsonToMap(t: String?):HashMap<String,String> {
        val map = HashMap<String, String>()
        val jObject = JSONObject(t)
        val keys: Iterator<*> = jObject.keys()
        while (keys.hasNext()) {
            val key = keys.next() as String
            val value = jObject.getString(key)
            map[key] = value
        }
        return map
    }

    fun formatText(et:EditText,s: Editable){

         val df: DecimalFormat = DecimalFormat("#,###.##")
         val dfnd: DecimalFormat = DecimalFormat("#,###")
         var hasFractionalPart: Boolean = false

        try {
            val inilen: Int = et.text?.length ?: 0
            val v: String = s.toString().replace(
                java.lang.String.valueOf(
                    df.decimalFormatSymbols.groupingSeparator
                ), ""
            )
            val n: Number = df.parse(v)
            val cp = et.selectionStart
            if (hasFractionalPart) {
                et.setText(df.format(n))
            } else {
                et.setText(dfnd.format(n))
            }
            val endlen: Int = et.text?.length ?: 0
            val sel = cp.plus((endlen - inilen))
            if (sel > 0 && sel <= et.text.length) {
                et.setSelection(sel)
            } else {
                // place cursor at the end?
                et.setSelection(et.text.length - 1)
            }
        } catch (nfe: NumberFormatException) {

        } catch (e: ParseException) {

        }
    }
}